# Write a report for a single demographic conflict (SPED Indicator/Level)

In this section we will write a basic report that compares a student's SPED indicator to their SPED level and returns an error if they aren't in agreement.  This will become the foundation of our Demographic Conflicts report.

## SQL Functions You Will Use

-   `SELECT`
-   `CASE`
-   `LIKE`
-   `LEFT JOIN`
-   `WHERE`

You have used all of these functions before except for `LIKE`.

## Step 1: Select the relevant columns

We begin with the basic skeleton of a SQL query: `SELECT`ing some columns from two tables, one of which is `JOIN`ed onto the other.

```sql
SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  usf.SPED_Indicator,
  usf.SPED_Level
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
```

## Step 2: Add the logic to find students with conflicting data

This query currently returns _all_ students, demographic conflicts or not.  What we want, though, are the students whose SPED Indicator is in conflict with their SPED Level.  We restrict queries using a `WHERE` clause, which is added to the _end_ of the query.

> **Technical Note:** The `WHERE` clause always comes after all of the `JOIN` clauses.

```sql
SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  usf.SPED_Indicator,
  usf.SPED_Level
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE (usf.SPED_Indicator = 0 AND usf.SPED_Level IS NOT NULL)
  OR (usf.SPED_Indicator = 1 AND usf.SPED_Level IS NULL)
```

> **Technical Note:** We need to group our logical statements using parentheses so that they are applied properly.

Our query now pulls only students who meet either of two criteria:
-   They are listed as **not SPED** (`SPED_Indicator = 0`) but have a **non-blank SPED Level** (`SPED_Level IS NOT NULL`)
-   They are listed as **SPED** (`SPED_Indicator = 1`) but have a **blank SPED Level** (`SPED_Level IS NULL`)

**Nota Bene:** It's possible that you get no results.  That just means none of your students are in conflict - good job.  As long as you receive no _errors_, though, you should be fine to continue.

## Step 3: Expand the scope of our search with the `LIKE` operator

This is a pretty good query, but we can actually make it a touch more specific.  SPED levels are supposed to be coded as `Level x`, but sometimes you'll see just a number, or occasionally a longform statement.  Right now, as long as `SPED_Level` isn't blank, all of these noncompliant values still pass.

Add the following line to our `WHERE` clause:

```sql
WHERE (usf.SPED_Indicator = 0 AND usf.SPED_Level IS NOT NULL)
  OR (usf.SPED_Indicator = 1 AND usf.SPED_Level IS NULL)
  OR (usf.SPED_Level NOT LIKE 'Level %')
```

The `LIKE` operator allows us to compare _parts_ of strings, versus using an `=` sign which would compare the _entire_ string.  The `%` sign is a **wildcard**, meaning it takes the place of one or more characters.

Give it a try - there's a good chance you'll end up with some additional students.

## Step 4: Create an error message

Just filtering down our students is lovely, but woefully inadequate for school personnel who are expected to actually _do_ something with the information.  Instead, we need to deliver an error message, and eventually a resolution path.

To do this, we will manipulate our `WHERE` clause into a `CASE` statement that delivers a pair of messages depending on the relevant error.

Add the following to your `SELECT` statement:

```sql
SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  usf.SPED_Indicator,
  usf.SPED_Level,
  CASE
    WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Student is flagged as SPED but has no SPED Level.'
    WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Student has a SPED Level but is not flagged as SPED.'
    WHEN SPED_Level NOT LIKE 'Level %' THEN 'Malformed SPED Level.'
    ELSE NULL
  END AS Error
```

> **Technical Note:** Plain text is enclosed in _single quotes_.

Now, instead of just returning the invalid data, we return an error message explaining _why_ the data is invalid.  Using the same method, we can create a resolution path.

## Step 5: Add a resolution path

Copy-paste your `CASE` statement from Step 4 and add change the prose to match the following:

```sql
CASE
  WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Student is flagged as SPED but has no SPED Level.'
  WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Student has a SPED Level but is not flagged as SPED.'
  WHEN SPED_Level NOT LIKE 'Level %' THEN 'Malformed SPED Level.'
  ELSE NULL
END AS Error,
CASE
  WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Either add a SPED Level or remove the SPED designation.'
  WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Either add a SPED designation or remove the SPED Level.'
  WHEN SPED_Level NOT LIKE 'Level %' THEN 'SPED Level should be of the form ''Level X''.'
  ELSE NULL
END AS ResolutionPath
```

> **Technical Note:** In order to display quotation marks in the result, we have to double up on the quote marks in the text.  That's why there's a strange-looking lump of quotes around _'Level X'_.

## Step 6: Swap out the `WHERE` clause and remove redundant columns

This isn't the end of the report - it's just part - so we need to do some cleanup that will make it play well with the rest of the report.

First, **remove** the columns `SPED_Indicator` and `SPED_Level` from the `SELECT` clause.  Since not every error involves these two columns, we have to remove them.

Next, **replace** the `WHERE` clause (which, now that we have error messages, is redundant) with

```sql
WHERE s.Enroll_Status = 0
```

which is our usual clause to include only currently-enrolled students.

## You have a functioning query!

If all went well, your query should look like

```sql
SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  CASE
    WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Student is flagged as SPED but has no SPED Level.'
    WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Student has a SPED Level but is not flagged as SPED.'
    WHEN SPED_Level NOT LIKE 'Level %' THEN 'Malformed SPED Level.'
    ELSE NULL
  END AS Error,
  CASE
    WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Either add a SPED Level or remove the SPED designation.'
    WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Either add a SPED designation or remove the SPED Level.'
    WHEN SPED_Level NOT LIKE 'Level %' THEN 'SPED Level should be of the form ''Level X''.'
    ELSE NULL
  END AS ResolutionPath
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE s.Enroll_Status = 0
```

Let's add to it in the next step.
