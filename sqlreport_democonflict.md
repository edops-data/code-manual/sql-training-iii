# Enter the query into SQLReports

In the last step, we added some tags to our Demographic Conflict query that make it behave better as a PowerSchool report.  Now, we will - finally - create the report in PowerSchool.

## Step 1: Navigate to SQLReports

Log into the admin portal of your favorite PowerSchool instance.  In the left-hand navigation menu, click on **System Reports** and then choose the **SQLReports** tab.

> **Note:**: Some installations will have a direct link to SQLReports on the home page.  That works too.

## Step 2: Create the new report

Scroll to the bottom of the page and click **Create a new SQLReport**.

Enter the following details:

| Field | Value |
| --- | --- |
| **Report Name** | Student Demographic Conflicts |
| **Report Title** | Student Demographic Conflicts |
| **Add After Report Title** | _leave blank_ |
| **Report Group** | _leave blank_ |
| **Report Description** | Listing of students with conflicting demographic fields. |

Under **SQL Query** paste the contents of the query you wrote in [Part 3](https://gitlab.com/edops-data/code-manual/sql-training-iii/blob/master/studentdemographicconflict.sql).

Under **Column Headings** click **Build Headings**.  You will end up with a set of 6 headers.  Name them:

-   Student Number
-   USI
-   Student Name
-   Grade Level
-   Error
-   Resolution Path

Click **Submit**.

## Step 3: Test drive the new report

Navigate to **Student Demographic Conflicts** and click **Submit**.  With luck, results will display.

## You now have a working report!

Enjoy a lovely beverage.
