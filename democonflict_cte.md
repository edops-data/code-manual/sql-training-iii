# Wrap the query in report-friendly elements

In the last step we produced a lengthly query that flags five different types of errors.  However, the query is a bit unwieldy - among other things, each subquery lists _every_ student, errors or not.

In this step, we'll wrap the query in some code to make it easier to work with, and do some housekeeping to prep it for deployment to PowerSchool.

## SQL Functions You Will Use

-   `WITH`
-   `INNER JOIN`
-   `WHERE`
-   `ORDER BY`

## PowerSchool DATs You Will Use

-   `~(curschoolid)`

## Step 1: Wrap the whole thing in a CTE to make it easier to handle

This is getting to be a massive query!  To make it easier to work with, let's bundle the whole thing together and give it a name.  Recall that the outcome of this process is called a **Common Table Expression**.

First, enclose your entire document in a pair of parentheses.  Then, in the first line of your document, add the text

```sql
WITH DemographicConflicts AS
```

In other words, your file will now look like:

```sql
WITH DemographicConflicts AS (
  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
...
UNION ALL
...
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE s.Enroll_Status = 0
)
```

Recall that the nice thing about CTEs is that we can treat them as tables, with all the rights and responsibilities that come with.

Add the following to the end of your file so that we can give it a test run:

```sql
SELECT dc.Student_Number,
  dc.USI,
  dc.LastFirst,
  dc.Grade_Level,
  dc.Error,
  dc.ResolutionPath
FROM DemographicConflicts dc
```

## Step 2: Tidy things up a bit

There are three remaining housekeeping steps before this query is ready to deploy:

-   Add a clause to restrict the schools being represented
-   Filter the list to only show students with errors
-   Sort the results by grade level and student

### Restrict the schools being represented

To accomplish the first point, we have our trusty `INNER JOIN` from earlier, which can be appended to the end of the file:

```sql
LEFT JOIN Students s ON s.Student_Number = dc.Student_Number
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
```

> **Technical Note:** Because `SchoolID` is not in our original query, we first have to join the `Students` table using `Student_Number` to get access to `Students.SchoolID` - hence the `LEFT JOIN` at the beginning of this code block.

### Filter the list to only show students with errors

To filter down the list, we can add a `WHERE` clause that only includes students who have errors.  Recalling that `WHERE` clauses go _after_ `JOIN` clauses, place the following at the _end_ of the file:

```sql
WHERE Error IS NOT NULL
```

### Sort the results by grade level and student

Recall that sorting is performed by an `ORDER BY` clause, which is the _last_ line of any query.  Place the following at the end of the file:

```sql
ORDER BY dc.Grade_Level ASC, dc.Student_Number ASC
```

## That's it!

Time to package this up into a PowerSchool report.  If you want to check your code, here's the entire beast of a query:

```sql
WITH DemographicConflicts AS (
  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Student is flagged as SPED but has no SPED Level.'
      WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Student has a SPED Level but is not flagged as SPED.'
      WHEN SPED_Level NOT LIKE 'Level %' THEN 'Malformed SPED Level.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Either add a SPED Level or remove the SPED designation.'
      WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Either add a SPED designation or remove the SPED Level.'
      WHEN SPED_Level NOT LIKE 'Level %' THEN 'SPED Level should be of the form ''Level X''.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Student is flagged as Homeless but has no Nighttime Residency.'
      WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Student has a Nighttime Residency but is not flagged as Homeless.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Either add a Nighttime Residency or remove the Homeless designation.'
      WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Either add a Homeless designation or remove the Nighttime Residency.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  LEFT JOIN StudentCoreFields scf ON scf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Students is flagged as Non-Resident but not Tuition Payer.'
      WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Student is flagged as Tuition Payer and also as Resident.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Confirm that the student is both non-resident and non-tuition paying.'
      WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Either remove the Residency or the Tuition Payer flag.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Students is flagged as New to US but does not have an Entry Code of 1851 or 1852.'
      WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Student has a New to US Entry Code (1851 or 1852) but is not flagged as New to US.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Either remove the New to US flag or change the Entry Code to 1851/1852.'
      WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Add the New to US flag.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Student has a Stage 4 Entry Code but a Stage 5 Exit Code.'
      WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Student has a Stage 5 Entry Code but a Stage 4 Exit Code.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Set the appropriate pair of Entry/Exit codes.'
      WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Set the appropriate pair of Entry/Exit codes.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0
)
SELECT dc.Student_Number,
  dc.USI,
  dc.LastFirst,
  dc.Grade_Level,
  dc.Error,
  dc.ResolutionPath
FROM DemographicConflicts dc
LEFT JOIN Students s ON s.Student_Number = dc.Student_Number
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
WHERE Error IS NOT NULL
ORDER BY dc.Grade_Level ASC, dc.Student_Number ASC
```
