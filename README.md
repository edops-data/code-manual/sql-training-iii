# SQL Training III

## Today's Goals:

-   Transform an existing SQL Query (Chronic Absenteeism from [SQL II](https://gitlab.com/edops-data/code-manual/sql-training-ii)) into a functioning SQLReport.

-   Create an additional SQL report from start to finish.  This query will identify students with demographic conflicts and propose resolution paths.

## Part 0: Preparation

You should have:

-   Git and Atom installed, and your school's repository cloned.  See Steps 0-2 at [SQL Training I](https://gitlab.com/edops-data/code-manual/sql/blob/master/README.md) for details.

-   A functioning copy of the Chronic Absenteeism query.  A [pre-built copy](./chronicabsent.sql) has been provided; details on its creation are at [SQL II](https://gitlab.com/edops-data/code-manual/sql-training-ii/blob/master/README.md).

-   An open APEX window, and an open SIS window that has navigated to **System Reports | SQLReports**

## Part 1: Add some parts to the Chronic Absenteeism query to make it friendlier with reporting

Details [here](./chronicabsent_sqlreport.md).

### Pre-Built Example

Note that this example assumes that your SIS uses `U_StudentsUserFields` as the extension table, which in 90% of cases is true.

```sql
WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT sch.Abbreviation AS School,
  s.Grade_Level,
  s.Student_Number,
  usf.USI,
  s.LastFirst,
  i.ISA
FROM ISA i
LEFT JOIN Students s ON s.ID = i.StudentID
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE i.ISA < 90
  AND i.YearID = ~(curyearid)
  AND s.Enroll_Status = 0
  AND s.EnrollmentCode = 0
ORDER BY s.Grade_Level ASC, i.ISA DESC, s.LastFirst ASC
```

## Part 2: Enter the query into SQLReports

Details [here](./sqlreport_chronicabsent.md).

### Pre-Built Example

To install this report, choose **Import a New sqlReport** and paste the contents into the box.

```xml
<ReportName>Chronic Absentee Listing</ReportName>
<ReportTitle>Chronic Absentee Listing</ReportTitle>
<AfterTitle></AfterTitle>
<ReportGroup>Attendance</ReportGroup>
<ReportDescription><textarea name="PF-RptDescr-0060122726">Listing of students who have missed 10% or more of enrolled days</textarea></ReportDescription>
<SQLQuery><textarea name="UF-0060052726">WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT sch.Abbreviation AS School,
  s.Grade_Level,
  s.Student_Number,
  usf.USI,
  s.LastFirst,
  i.ISA
FROM ISA i
LEFT JOIN Students s ON s.ID = i.StudentID
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE i.ISA &lt; 90
  AND i.YearID = ~(curyearid)
  AND s.Enroll_Status = 0
  AND s.EnrollmentCode = 0
ORDER BY s.Grade_Level ASC, i.ISA DESC, s.LastFirst ASC
</textarea></SQLQuery>
<ReportHeader><th>School</th><th>Grade Level</th><th>Student Number</th><th>Student Name</th><th>ISA (%)</th></ReportHeader>
<CreateStudentSelection>0</CreateStudentSelection>
<StudentSelectionQuery></StudentSelectionQuery>
<CreateStudentSelectionB></CreateStudentSelectionB>
<IncludeRowNumber>0</IncludeRowNumber>
<OpeninNewWindow>0</OpeninNewWindow>
<HideCopyButton></HideCopyButton>
<HideCSVButton></HideCSVButton>
<HideTabButton></HideTabButton>
<HidePDFButton></HidePDFButton>
<ShowSearchBox></ShowSearchBox>
<ShowResultsInfo></ShowResultsInfo>
<ParameterName1></ParameterName1>
<ParameterVal1></ParameterVal1>
<ParameterCal1></ParameterCal1>
<ParameterOpt1></ParameterOpt1>
<ParameterName2></ParameterName2>
<ParameterVal2></ParameterVal2>
<ParameterCal2></ParameterCal2>
<ParameterOpt2></ParameterOpt2>
<ParameterName3></ParameterName3>
<ParameterVal3></ParameterVal3>
<ParameterCal3></ParameterCal3>
<ParameterOpt3></ParameterOpt3>
<ParameterName4></ParameterName4>
<ParameterVal4></ParameterVal4>
<ParameterCal4></ParameterCal4>
<ParameterOpt4></ParameterOpt4>
<ParameterName5></ParameterName5>
<ParameterVal5></ParameterVal5>
<ParameterCal5></ParameterCal5>
<ParameterOpt5></ParameterOpt5>
<PdfOrientation></PdfOrientation>
<NoCSVQuotes></NoCSVQuotes>
<NoRowSel></NoRowSel>
<ShowSHC></ShowSHC>
<UseColRe></UseColRe>
<UseFixHdr></UseFixHdr>
<HidePrint></HidePrint>
<ReportNotes><textarea name="PF-ReportNotes-0060122726"></textarea></ReportNotes>
<UseColFilt></UseColFilt>
<UseRowGroups></UseRowGroups>
<UseRowGroupsOptions>Opt1</UseRowGroupsOptions>
<DisplayRowGroupsCol></DisplayRowGroupsCol>
<UseRowGroupsCounts></UseRowGroupsCounts>
<sqlChartsReport></sqlChartsReport>
<InitialsqlChart></InitialsqlChart>
<InitialChartName></InitialChartName>
<RowGroupsDesc></RowGroupsDesc>
<FLSMapField></FLSMapField>
<ExcFrmList></ExcFrmList>
<ExecGrp></ExecGrp>
<ValueLi3>0</ValueLi3>
<NoHeaderRow></NoHeaderRow>
<ReportDirections><textarea name="PF-RptDirections-0060122726"></textarea></ReportDirections>
<HideParams></HideParams>
<ShowGridlines></ShowGridlines>
<ValueR>0</ValueR>
<NoFooterRow></NoFooterRow>
```

## Part 3A: Write a report for a single demographic conflict (SPED Indicator/Level)

Details [here](./democonflict_single.md).

### Pre-Built Example

```sql
SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  CASE
    WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Student is flagged as SPED but has no SPED Level.'
    WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Student has a SPED Level but is not flagged as SPED.'
    WHEN SPED_Level NOT LIKE 'Level %' THEN 'Malformed SPED Level.'
    ELSE NULL
  END AS Error,
  CASE
    WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Either add a SPED Level or remove the SPED designation.'
    WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Either add a SPED designation or remove the SPED Level.'
    WHEN SPED_Level NOT LIKE 'Level %' THEN 'SPED Level should be of the form ''Level X''.'
    ELSE NULL
  END AS ResolutionPath
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE s.Enroll_Status = 0
```

## Part 3B: Add on a bunch of other demographic conflicts with `UNION`

Details [here](./democonflict_multiple.md).

### Pre-Built Example

```sql
SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  CASE
    WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Student is flagged as SPED but has no SPED Level.'
    WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Student has a SPED Level but is not flagged as SPED.'
    WHEN SPED_Level NOT LIKE 'Level %' THEN 'Malformed SPED Level.'
    ELSE NULL
  END AS Error,
  CASE
    WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Either add a SPED Level or remove the SPED designation.'
    WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Either add a SPED designation or remove the SPED Level.'
    WHEN SPED_Level NOT LIKE 'Level %' THEN 'SPED Level should be of the form ''Level X''.'
    ELSE NULL
  END AS ResolutionPath
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE s.Enroll_Status = 0

UNION ALL

SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  CASE
    WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Student is flagged as Homeless but has no Nighttime Residency.'
    WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Student has a Nighttime Residency but is not flagged as Homeless.'
    ELSE NULL
  END AS Error,
  CASE
    WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Either add a Nighttime Residency or remove the Homeless designation.'
    WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Either add a Homeless designation or remove the Nighttime Residency.'
    ELSE NULL
  END AS ResolutionPath
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
LEFT JOIN StudentCoreFields scf ON scf.StudentsDCID = s.DCID
WHERE s.Enroll_Status = 0

UNION ALL

SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  CASE
    WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Students is flagged as Non-Resident but not Tuition Payer.'
    WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Student is flagged as Tuition Payer and also as Resident.'
    ELSE NULL
  END AS Error,
  CASE
    WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Confirm that the student is both non-resident and non-tuition paying.'
    WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Either remove the Residency or the Tuition Payer flag.'
    ELSE NULL
  END AS ResolutionPath
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE s.Enroll_Status = 0

UNION ALL

SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  CASE
    WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Students is flagged as New to US but does not have an Entry Code of 1851 or 1852.'
    WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Student has a New to US Entry Code (1851 or 1852) but is not flagged as New to US.'
    ELSE NULL
  END AS Error,
  CASE
    WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Either remove the New to US flag or change the Entry Code to 1851/1852.'
    WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Add the New to US flag.'
    ELSE NULL
  END AS ResolutionPath
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE s.Enroll_Status = 0

UNION ALL

SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
  CASE
    WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Student has a Stage 4 Entry Code but a Stage 5 Exit Code.'
    WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Student has a Stage 5 Entry Code but a Stage 4 Exit Code.'
    ELSE NULL
  END AS Error,
  CASE
    WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Set the appropriate pair of Entry/Exit codes.'
    WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Set the appropriate pair of Entry/Exit codes.'
    ELSE NULL
  END AS ResolutionPath
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE s.Enroll_Status = 0
```

## Part 3C: Wrap the query in report-friendly elements

Details [here](./democonflict_cte.md).

### Pre-Built Example

```sql
WITH DemographicConflicts AS (
  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Student is flagged as SPED but has no SPED Level.'
      WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Student has a SPED Level but is not flagged as SPED.'
      WHEN SPED_Level NOT LIKE 'Level %' THEN 'Malformed SPED Level.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Either add a SPED Level or remove the SPED designation.'
      WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Either add a SPED designation or remove the SPED Level.'
      WHEN SPED_Level NOT LIKE 'Level %' THEN 'SPED Level should be of the form ''Level X''.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Student is flagged as Homeless but has no Nighttime Residency.'
      WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Student has a Nighttime Residency but is not flagged as Homeless.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Either add a Nighttime Residency or remove the Homeless designation.'
      WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Either add a Homeless designation or remove the Nighttime Residency.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  LEFT JOIN StudentCoreFields scf ON scf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Students is flagged as Non-Resident but not Tuition Payer.'
      WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Student is flagged as Tuition Payer and also as Resident.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Confirm that the student is both non-resident and non-tuition paying.'
      WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Either remove the Residency or the Tuition Payer flag.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Students is flagged as New to US but does not have an Entry Code of 1851 or 1852.'
      WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Student has a New to US Entry Code (1851 or 1852) but is not flagged as New to US.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Either remove the New to US flag or change the Entry Code to 1851/1852.'
      WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Add the New to US flag.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Student has a Stage 4 Entry Code but a Stage 5 Exit Code.'
      WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Student has a Stage 5 Entry Code but a Stage 4 Exit Code.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Set the appropriate pair of Entry/Exit codes.'
      WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Set the appropriate pair of Entry/Exit codes.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0
)
SELECT dc.Student_Number,
  dc.USI,
  dc.LastFirst,
  dc.Grade_Level,
  dc.Error,
  dc.ResolutionPath
FROM DemographicConflicts dc
LEFT JOIN Students s ON s.Student_Number = dc.Student_Number
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
WHERE Error IS NOT NULL
ORDER BY dc.Grade_Level ASC, dc.Student_Number ASC
```

## Part 3D: Enter the query into SQLReports

Details [here](./sqlreport_democonflict.md).

### Pre-Built Example

```xml

<ReportName>Demographic Conflicts</ReportName>
<ReportTitle>Demographic Conflicts</ReportTitle>
<AfterTitle></AfterTitle>
<ReportGroup></ReportGroup>
<ReportDescription><textarea name="PF-RptDescr-0060122727">Identifies conflicting student demographic information</textarea></ReportDescription>
<SQLQuery><textarea name="UF-0060052727">SELECT dc.Student_Number,
  dc.USI,
  dc.LastFirst,
  dc.Grade_Level,
  dc.Error,
  dc.ResolutionPath
FROM (
  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Student is flagged as SPED but has no SPED Level.'
      WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Student has a SPED Level but is not flagged as SPED.'
      WHEN SPED_Level NOT LIKE 'Level %' THEN 'Malformed SPED Level.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Either add a SPED Level or remove the SPED designation.'
      WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Either add a SPED designation or remove the SPED Level.'
      WHEN SPED_Level NOT LIKE 'Level %' THEN 'SPED Level should be of the form ''Level X''.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Student is flagged as Homeless but has no Nighttime Residency.'
      WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Student has a Nighttime Residency but is not flagged as Homeless.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Either add a Nighttime Residency or remove the Homeless designation.'
      WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Either add a Homeless designation or remove the Nighttime Residency.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  LEFT JOIN StudentCoreFields scf ON scf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.Residency_Status = 0 AND s.TuitionPayer &lt;&gt; 2 THEN 'Students is flagged as Non-Resident but not Tuition Payer.'
      WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Student is flagged as Tuition Payer and also as Resident.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.Residency_Status = 0 AND s.TuitionPayer &lt;&gt; 2 THEN 'Confirm that the student is both non-resident and non-tuition paying.'
      WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Either remove the Residency or the Tuition Payer flag.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Students is flagged as New to US but does not have an Entry Code of 1851 or 1852.'
      WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Student has a New to US Entry Code (1851 or 1852) but is not flagged as New to US.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Either remove the New to US flag or change the Entry Code to 1851/1852.'
      WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Add the New to US flag.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Student has a Stage 4 Entry Code but a Stage 5 Exit Code.'
      WHEN s.EntryCode &lt;&gt; 1800 AND s.ExitCode IN (1851, 1852) THEN 'Student has a Stage 5 Entry Code but a Stage 4 Exit Code.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Set the appropriate pair of Entry/Exit codes.'
      WHEN s.EntryCode &lt;&gt; 1800 AND s.ExitCode IN (1851, 1852) THEN 'Set the appropriate pair of Entry/Exit codes.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0
) dc
LEFT JOIN Students s ON s.Student_Number = dc.Student_Number
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
WHERE Error IS NOT NULL
ORDER BY dc.Grade_Level ASC, dc.Student_Number ASC
</textarea></SQLQuery>
<ReportHeader><th>Student Number</th><th>USI</th><th>Name</th><th>Grade Level</th><th>Error</th><th>Resolution Path</th></ReportHeader>
<CreateStudentSelection>0</CreateStudentSelection>
<StudentSelectionQuery></StudentSelectionQuery>
<CreateStudentSelectionB></CreateStudentSelectionB>
<IncludeRowNumber>0</IncludeRowNumber>
<OpeninNewWindow>0</OpeninNewWindow>
<HideCopyButton></HideCopyButton>
<HideCSVButton></HideCSVButton>
<HideTabButton></HideTabButton>
<HidePDFButton></HidePDFButton>
<ShowSearchBox></ShowSearchBox>
<ShowResultsInfo></ShowResultsInfo>
<ParameterName1></ParameterName1>
<ParameterVal1></ParameterVal1>
<ParameterCal1></ParameterCal1>
<ParameterOpt1></ParameterOpt1>
<ParameterName2></ParameterName2>
<ParameterVal2></ParameterVal2>
<ParameterCal2></ParameterCal2>
<ParameterOpt2></ParameterOpt2>
<ParameterName3></ParameterName3>
<ParameterVal3></ParameterVal3>
<ParameterCal3></ParameterCal3>
<ParameterOpt3></ParameterOpt3>
<ParameterName4></ParameterName4>
<ParameterVal4></ParameterVal4>
<ParameterCal4></ParameterCal4>
<ParameterOpt4></ParameterOpt4>
<ParameterName5></ParameterName5>
<ParameterVal5></ParameterVal5>
<ParameterCal5></ParameterCal5>
<ParameterOpt5></ParameterOpt5>
<PdfOrientation></PdfOrientation>
<NoCSVQuotes></NoCSVQuotes>
<NoRowSel></NoRowSel>
<ShowSHC></ShowSHC>
<UseColRe></UseColRe>
<UseFixHdr></UseFixHdr>
<HidePrint></HidePrint>
<ReportNotes><textarea name="PF-ReportNotes-0060122727"></textarea></ReportNotes>
<UseColFilt></UseColFilt>
<UseRowGroups></UseRowGroups>
<UseRowGroupsOptions>Opt1</UseRowGroupsOptions>
<DisplayRowGroupsCol></DisplayRowGroupsCol>
<UseRowGroupsCounts></UseRowGroupsCounts>
<sqlChartsReport></sqlChartsReport>
<InitialsqlChart></InitialsqlChart>
<InitialChartName></InitialChartName>
<RowGroupsDesc></RowGroupsDesc>
<FLSMapField></FLSMapField>
<ExcFrmList></ExcFrmList>
<ExecGrp></ExecGrp>
<ValueLi3>0</ValueLi3>
<NoHeaderRow></NoHeaderRow>
<ReportDirections><textarea name="PF-RptDirections-0060122727"></textarea></ReportDirections>
<HideParams></HideParams>
<ShowGridlines></ShowGridlines>
<ValueR>0</ValueR>
<NoFooterRow></NoFooterRow>
```
