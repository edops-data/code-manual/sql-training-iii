WITH DemographicConflicts AS (
  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Student is flagged as SPED but has no SPED Level.'
      WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Student has a SPED Level but is not flagged as SPED.'
      WHEN SPED_Level NOT LIKE 'Level %' THEN 'Malformed SPED Level.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN SPED_Indicator = 1 AND SPED_Level IS NULL THEN 'Either add a SPED Level or remove the SPED designation.'
      WHEN SPED_Indicator = 0 AND SPED_Level IS NOT NULL THEN 'Either add a SPED designation or remove the SPED Level.'
      WHEN SPED_Level NOT LIKE 'Level %' THEN 'SPED Level should be of the form ''Level X''.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Student is flagged as Homeless but has no Nighttime Residency.'
      WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Student has a Nighttime Residency but is not flagged as Homeless.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Either add a Nighttime Residency or remove the Homeless designation.'
      WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Either add a Homeless designation or remove the Nighttime Residency.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  LEFT JOIN StudentCoreFields scf ON scf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Students is flagged as Non-Resident but not Tuition Payer.'
      WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Student is flagged as Tuition Payer and also as Resident.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Confirm that the student is both non-resident and non-tuition paying.'
      WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Either remove the Residency or the Tuition Payer flag.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Students is flagged as New to US but does not have an Entry Code of 1851 or 1852.'
      WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Student has a New to US Entry Code (1851 or 1852) but is not flagged as New to US.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Either remove the New to US flag or change the Entry Code to 1851/1852.'
      WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Add the New to US flag.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0

  UNION ALL

  SELECT s.Student_Number,
    usf.USI,
    s.LastFirst,
    s.Grade_Level,
    CASE
      WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Student has a Stage 4 Entry Code but a Stage 5 Exit Code.'
      WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Student has a Stage 5 Entry Code but a Stage 4 Exit Code.'
      ELSE NULL
    END AS Error,
    CASE
      WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Set the appropriate pair of Entry/Exit codes.'
      WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Set the appropriate pair of Entry/Exit codes.'
      ELSE NULL
    END AS ResolutionPath
  FROM Students s
  LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
  WHERE s.Enroll_Status = 0
)
SELECT dc.Student_Number,
  dc.USI,
  dc.LastFirst,
  dc.Grade_Level,
  dc.Error,
  dc.ResolutionPath
FROM DemographicConflicts dc
LEFT JOIN Students s ON s.Student_Number = dc.Student_Number
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
WHERE Error IS NOT NULL
ORDER BY dc.Grade_Level ASC, dc.Student_Number ASC
