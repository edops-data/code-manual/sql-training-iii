# Add on a bunch of other demographic conflicts with `UNION`

In the last step, we created a query that returns an error message and resolution path for a specific demographic conflict.  In this step, we will repeat the process for several other demographic conflicts and then use a `UNION` operator to link them all together.

## SQL Functions You Will Use

-   `UNION ALL`
-   `IN`

## Step 1: Duplicate the original query and attach it with a `UNION`

Just like how `JOIN`s expand a table _horizontally_ by gluing _tables_ together, a `UNION` expands a table _vertically_ by gluing _queries_ together.

A `UNION` has four conditions to work:

1.  The **number of columns** must be the same in both queries;
2.  The **column names** must be the same in both queries;
3.  The **column orders** must be the same in both queries; and
4.  The **data types** must be the same in both queries.

Item number 2 is why we had to remove our SPED columns earlier on.

Beneath your query from Part 3A, add the line `UNION ALL` and then copy-paste another copy of that same query.  It should look like:

```sql
...
FROM Students s
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
WHERE s.Enroll_Status = 0

UNION ALL

SELECT s.Student_Number,
  usf.USI,
  s.LastFirst,
  s.Grade_Level,
...
```

When you run the query, you probably won't notice anything different since the duplicate set of rows are appended to the bottom of the query, and anything after the first 20 rows are suppressed anyway.  But it's there.

## Step 2: Update the new query to check Homeless status

This second query is going to check for conflicts in a student's Homeless information, namely whether:

-   A student is flagged as homeless (`Homeless_Indicator = 1`) but Nighttime Residency is blank (`Homeless_Code IS NULL`)
-   A student is flagged a not homeless (`Homeless_Indicator = 0`) but Nighttime Residency contains information (`Homeless_Code IS NOT NULL`)

Update the `CASE` statements to read:

```sql
CASE
  WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Student is flagged as Homeless but has no Nighttime Residency.'
  WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Student has a Nighttime Residency but is not flagged as Homeless.'
  ELSE NULL
END AS Error,
CASE
  WHEN usf.Homeless_Indicator = 1 AND scf.Homeless_Code IS NULL THEN 'Either add a Nighttime Residency or remove the Homeless designation.'
  WHEN usf.Homeless_Indicator = 0 AND scf.Homeless_Code IS NOT NULL THEN 'Either add a Homeless designation or remove the Nighttime Residency.'
  ELSE NULL
END AS ResolutionPath
```

## Step 3: Add a few more queries

Why stop now?  Repeat Steps 1 and 2 (Add with a `UNION` and update the `CASE`) for the following (only the `CASE` statements are included, but the entire query needs to be copied.)

### Residency vs. Tuition

```sql
CASE
  WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Students is flagged as Non-Resident but not Tuition Payer.'
  WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Student is flagged as Tuition Payer and also as Resident.'
  ELSE NULL
END AS Error,
CASE
  WHEN usf.Residency_Status = 0 AND s.TuitionPayer <> 2 THEN 'Confirm that the student is both non-resident and non-tuition paying.'
  WHEN usf.Residency_Status = 1 AND s.TuitionPayer = 2 THEN 'Either remove the Residency or the Tuition Payer flag.'
  ELSE NULL
END AS ResolutionPath
```

> **Technical Note:** The SQL operator for 'Not Equals' is `<>`.  Go figure.

### New to US vs. Entry Code

```sql
CASE
  WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Students is flagged as New to US but does not have an Entry Code of 1851 or 1852.'
  WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Student has a New to US Entry Code (1851 or 1852) but is not flagged as New to US.'
  ELSE NULL
END AS Error,
CASE
  WHEN usf.New_To_US = 1 AND s.EntryCode NOT IN (1851, 1852) THEN 'Either remove the New to US flag or change the Entry Code to 1851/1852.'
  WHEN usf.New_To_US = 0 AND s.EntryCode IN (1851, 1852) THEN 'Add the New to US flag.'
  ELSE NULL
END AS ResolutionPath
```

Since we're comparing the Entry Code to two distinct values, we don't use `LIKE`.  Instead, we use `IN` and place the elements we're comparing against in a set.

### Stage 4/5 Conflict

```sql
CASE
  WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Student has a Stage 4 Entry Code but a Stage 5 Exit Code.'
  WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Student has a Stage 5 Entry Code but a Stage 4 Exit Code.'
  ELSE NULL
END AS Error,
CASE
  WHEN s.EntryCode = 1800 AND s.ExitCode NOT IN (1234, 4321) THEN 'Set the appropriate pair of Entry/Exit codes.'
  WHEN s.EntryCode <> 1800 AND s.ExitCode IN (1851, 1852) THEN 'Set the appropriate pair of Entry/Exit codes.'
  ELSE NULL
END AS ResolutionPath
```

## That's it!

You now have a functioning, if unwieldy, query.  In the next step we'll tidy things up a touch in preparation for creating a PowerSchool report.
