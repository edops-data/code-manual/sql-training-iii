WITH
AttendanceCoded AS (
  SELECT att.StudentID,
    att.YearID,
    att.Att_Date,
    att.Att_Mode_Code,
    (CASE
      WHEN Presence_Status_CD = 'Present' THEN 1
      ELSE 0
     END) AS Present
  FROM Attendance att
  LEFT JOIN Attendance_Code attc ON attc.ID = att.Attendance_CodeID
),
ISA AS (
  SELECT StudentID,
    YearID,
    COUNT(Att_Date) AS DaysEnrolled,
    SUM(Present) AS DaysPresent,
    ROUND(SUM(Present) / COUNT(Att_Date) * 100, 2) AS ISA
  FROM AttendanceCoded
  WHERE Att_Mode_Code = 'ATT_ModeDaily'
  GROUP BY StudentID, YearID
)
SELECT sch.Abbreviation AS School,
  s.Grade_Level,
  s.Student_Number,
  usf.USI,
  s.LastFirst,
  i.ISA
FROM ISA i
LEFT JOIN Students s ON s.ID = i.StudentID
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
WHERE i.ISA < 90
  AND i.YearID = ~(curyearid)
  AND s.Enroll_Status = 0
  AND s.EnrollmentCode = 0
ORDER BY s.Grade_Level ASC, i.ISA DESC, s.LastFirst ASC
