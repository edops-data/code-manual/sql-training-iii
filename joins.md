# Joins: A Detailed Treatise

## The Basics: Imagining a Join

A `JOIN` is how SQL attaches one table to another.  However, instead of thinking about tables, it's perhaps easier to think about records.

Consider the simple query, which returns the date, Student ID, and Student Name for every attendance record in the system:

```sql
SELECT att.Att_Date,
  att.StudentID
  s.LastFirst
FROM Attendance att
LEFT JOIN Students s ON s.ID = att.StudentID
```

In this case, the join is like the user saying, _for **each record** in the `Attendance` table, give me the corresponding `Student` record.  You'll know it's the right record because its `ID` field will match `StudentID` from the Attendance table._

This works well enough because `ID` is uniquely defined in the `Students` table; i.e. there is one and only one record for each Student ID.

## Multiple Conditions

Sometimes, though, we need multiple conditions.  Consider this example, which pulls any attendance records that predate the first day of school.  Not only do we need the right year, but we also need the right campus, since different campuses often have different start dates:

```sql
SELECT att.Att_Date,
  att.StudentID
FROM Attendance att
LEFT JOIN Terms ter ON ter.YearID = att.YearID
  AND ter.SchoolID = att.SchoolID
WHERE att.Att_Date < ter.FirstDay
```

It may help to see this with actual data.  Consider the tables of students and terms below:

#### Attendance

| Att_Date | StudentID | SchoolID |
| --- | --- | --- |
| 8/1/2019 | 12345 | 900 |
| 8/15/2019 | 12345 | 900 |
| 8/15/2019 | 76543 | 901 |
| 8/20/2019 | 76543 | 901 |

#### Terms

| YearID | SchoolYear | SchoolID |
| --- | --- | --- |
| 29 | 2019 | 900 |
| 28 | 2018 | 900 |
| 29 | 2019 | 901 |
| 28 | 2018 | 901 |

If we just joined on `YearID` and failed to specify `SchoolID`, each student would end up with _two_ records; one from school `900` and another from school `901`:

| Att_Date | StudentID | SchoolID (Attendance Table) | YearID | SchoolYear | SchoolID (Terms Table) |
| --- | --- | --- | --- | --- | --- |
| 8/1/2019 | 12345 | **900** | 29 | 2019 | **900** |
| 8/1/2019 | 12345 | **900** | 29 | 2019 | **901** |
| 8/15/2019 | 12345 | 900 | 29 | 2019 | 900 |
| 8/15/2019 | 12345 | 900 | 29 | 2019 | 901 |
| 8/15/2019 | 76543 | 901 | 29 | 2019 | 900 |
| 8/15/2019 | 76543 | 901 | 29 | 2019 | 901 |
| 8/20/2019 | 76543 | 901 | 29 | 2019 | 900 |
| 8/20/2019 | 76543 | 901 | 29 | 2019 | 901 |

Instead, we need to specify **both** the `YearID` and the `SchoolID` in order to get a unique record.

## LEFT vs INNER Joins

So far, all of our joins have been `LEFT JOIN`s.  The other type of join that we're concerned with is the `INNER JOIN`.  (There is also a Right join but under normal circumstances you will never need it.)

The difference is straightforward in principle, if nuanced in practice.  In short:

-   `X LEFT JOIN Y` includes _all rows_ from table `X`, and matching rows from table `Y`.

-   `X INNER JOIN Y` includes only rows that match _both tables_.

By way of example, consider these two tables:

#### Advisories

| House | Advisor |
| --- | --- |
| Gryffindor | Minerva McGonagall |
| Ravenclaw | Filius Flitwick |
| Hufflepuff | Pomona Sprout |
| Slytherin | Severus Snape |

#### Students

| Student | House |
| --- | --- |
| Ron Weasley | Gryffindor |
| Harry Potter | Gryffindor |
| Cedric Diggory | Hufflepuff |
| Draco Malfoy | Slytherin |
| Vincent Crabbe | Slytherin |

### `LEFT JOIN`

The code
```sql
SELECT *
FROM Advisory a
LEFT JOIN Students s ON s.House = a.House
```

will show _every row_ from the `Advisories` table, plus matching rows from the `Students` table.  Where there is no matching row, a `NULL` will be returned.

| House | Advisor | Student | House |
| --- | --- | --- | --- |
| Gryffindor | Minerva McGonagall | Ron Weasley | Gryffindor |
| Gryffindor | Minerva McGonagall | Harry Potter | Gryffindor |
| Ravenclaw | Filius Flitwick | **NULL** | **NULL** |
| Hufflepuff | Pomona Sprout | Cedric Diggory | Hufflepuff |
| Slytherin | Severus Snape | Draco Malfoy | Slytherin |
| Slytherin | Severus Snape | Vincent Crabbe | Slytherin |

### `INNER JOIN`

The code
```sql
SELECT *
FROM Advisory a
INNER JOIN Students s ON s.House = a.House
```

will return the same table as the `LEFT JOIN`, except that any rows _without a match_ will be _rejected_.  In this case, that means Ravenclaw, which has no students in the `Students` table, will be dropped.

| House | Advisor | Student | House |
| --- | --- | --- | --- |
| Gryffindor | Minerva McGonagall | Ron Weasley | Gryffindor |
| Gryffindor | Minerva McGonagall | Harry Potter | Gryffindor |
| Hufflepuff | Pomona Sprout | Cedric Diggory | Hufflepuff |
| Slytherin | Severus Snape | Draco Malfoy | Slytherin |
| Slytherin | Severus Snape | Vincent Crabbe | Slytherin |
