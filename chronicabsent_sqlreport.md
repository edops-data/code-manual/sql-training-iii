# Add some parts to the Chronic Absenteeism query to make it friendlier with reporting

Our Chronic Absenteeism report works great from the perspective of an omniscient Data Specialist, but it has a few drawbacks when used by school personnel, namely:

-   The school year is hard-coded, which will pose a problem come August.
-   Students are not separated by campus, which can pose confusion or lead to inaccurate data.

We can fix both issues using a combination of DAT*s and a clever function.

\***DAT** = Data Access Tag, a notation for some internal PowerSchool functions.

## SQL Functions You Will Use

-   `INNER JOIN`

## PowerSchool DATs You Will Use

-   `~(curyearid)`
-   `~(curschoolid)`

## Step 1: Add the USI

USI is stored in an extension table, which means we have to attach that table to our query.  Recall that we attach tables using a `JOIN` clause.  (Specifically, a `LEFT JOIN` - the only type of join we know how to use at present.)

### 1A: LEFT JOIN the extension table
In your query, find the line where we joined the `Students` table, and beneath it add:

```sql
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
```

So the end of your query now looks like
```sql
...
FROM ISA i
LEFT JOIN Students s ON s.ID = i.StudentID
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
WHERE i.ISA < 90
...
```

> **Nota Bene**: In some cases (Adam will let you know), your extension table will be `U_DEF_EXT_STUDENTS`.  For simplicity, keep `usf` as the alias.

### 1B: Add the `USI` to the `SELECT` statement

We've added the extension table, but we haven't pulled any data from it.  Let's fix that.

In the `SELECT` clause of your query, add `usf.USI` to the list of columns being pulled, right after `Student_Number`. I.e.:

```sql
SELECT sch.Abbreviation AS School,
  s.Grade_Level,
  s.Student_Number,
  usf.USI,
  s.LastFirst,
  i.ISA
```

## Step 2: Replace the `YearID` with a DAT

The `~(curyearid)` DAT returns the 2-digit year code for whatever year is **selected** in the drop-down at the top of the PowerSchool screen.  This is useful for two reasons:

1.  It lets the same report work from year to year with no change to code
2.  It lets the user go back in time and look at results from prior years (with varying degrees of success)

At the end of your Chronic Absenteeism query, change the line

```sql
AND i.YearID = 29
```

to read

```sql
AND i.YearID = ~(curyearid)
```

> **Unfortunate Note:** APEX doesn't support DATs, so you can no longer test your code therein.  From here out, everything operates on faith and a steady hand.

## Step 3: Restrict the listing based on the selected school with an `INNER JOIN`

We're going to use a handy bit of code to restrict the listing based on the school that is currently selected:

```sql
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
```

It looks a bit obscure, but let's see if we can figure out what it does.

### ~(curschoolid)

This is another DAT, one which returns the ID of the school currently selected.  If the **District Office** is selected, it returns a `0`.

### CASE Statement

Recall that a `CASE` is basically an if-then statment.  If we're in the District Office (i.e. `~(curschoolid) = 0`), then we return all available values for `School_Number`.  Otherwise, all we return is our current school ID.

### SELECT DISTINCT

Whether the query is returning all school IDs or just the one, it's going to return one value per school in the `Schools` table.  In other words, if we have 5 schools in our district and we're currently in the Middle School, we're going to get 5 results, even if they've all been set to the Middle School's ID.  Adding the word `DISTINCT` to our `SELECT` statement removes these unwanted duplicates.

### INNER JOIN

By using an `INNER JOIN` we eliminate any students from our list who are not from the desired school.

> **Pedantry Alert:** If you're really interested, see this [detailed treatise](./joins.md) on joins.

#### TL;DR:

-   The statement `X LEFT JOIN Y` will keep _all the rows_ in table `X` and attach any matching rows from Table `Y`.

-   The statement `X INNER JOIN Y` will keep _only the rows_ that are in table `X` _and_ in table `Y`.  In that sense, we can use it as a filter.

#### In any case...

Insert this code block _after_ the `U_StudentsUserFields` join but _before_ the `Schools` join.  You should end up with:

```sql
LEFT JOIN Students s ON s.ID = i.StudentID
LEFT JOIN U_StudentsUserFields usf ON usf.StudentsDCID = s.DCID
INNER JOIN (SELECT DISTINCT CASE
                              WHEN ~(curschoolid) = 0 THEN School_Number
                              ELSE ~(curschoolid)
                            END AS School_Number
            FROM Schools) ThisSchool ON ThisSchool.School_Number = s.SchoolID
LEFT JOIN Schools sch ON sch.School_Number = s.SchoolID
```

## You now have a query that's road-ready!

Time to get it into PowerSchool.  But that's Part 2.
