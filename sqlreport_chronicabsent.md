# Enter the query into SQLReports

In the last step, we added some tags to our Chronic Absenteeism query that make it behave better as a PowerSchool report.  Now, we will - finally - create the report in PowerSchool.

## Step 1: Navigate to SQLReports

Log into the admin portal of your favorite PowerSchool instance.  In the left-hand navigation menu, click on **System Reports** and then choose the **SQLReports** tab.

> **Note:**: Some installations will have a direct link to SQLReports on the home page.  That works too.

## Step 2: Create the new report

Scroll to the bottom of the page and click **Create a new SQLReport**.

Enter the following details:

| Field | Value |
| --- | --- |
| **Report Name** | Chronic Absentee Listing |
| **Report Title** | Chronic Absentee Listing |
| **Add After Report Title** | _leave blank_ |
| **Report Group** | Attendance |
| **Report Description** | Listing of students who are chronically absent, defined as having missed 10% or more of their enrolled days year-to-date. |

Under **SQL Query** paste the contents of the query you wrote in [Part 1](https://gitlab.com/edops-data/code-manual/sql-training-iii/blob/master/chronicabsent_sqlreport.sql).

Under **Column Headings** click **Build Headings**.  You will end up with a set of 6 headers.  Name them:

-   School
-   Grade Level
-   Student Number
-   USI
-   Student Name
-   ISA (%)

Click **Submit**.

## Step 3: Test drive the new report

Navigate to **Attendance** | **Chronic Absentee Listing** and click **Submit**.  With luck, results will display.

## You now have a working report!

Give yourself a hug... and now let's do the whole thing again in Part 3 :)
